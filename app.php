<?php
require "controllers/OrderController.php";

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );
if ((isset($uri[2]) && $uri[2] != 'create') || !isset($uri[3])) {
    header("HTTP/1.1 404 Not Found");
    exit();
}

$objFeedController = new OrderController();
$strMethodName = $uri[3] . 'Action';
$objFeedController->{$strMethodName}();

