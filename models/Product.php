<?php

class Product
{
    private $conn;
    private $table_name = 'products';

    public $id;
    public $name;

    public function __construct($db)
    {
        $this->conn = $db;
    }
}