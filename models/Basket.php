<?php

class Basket
{
    private $conn;
    private $table_name = 'product_in_basket';

    public $userId;
    public $productId;
    public $qty;

    public function __construct($db)
    {
        $this->conn = $db;
    }
}