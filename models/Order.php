<?php

class Order
{
    private $conn;
    private $table_name = 'orders';

    public $id;
    public $userId;
    public $address;
    public $status;

    public function __construct($db)
    {
        $this->conn = $db;
    }
}