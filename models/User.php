<?php

class User
{
    private $conn;
    private $table_name = 'users';

    public $id;
    public $username;
    public $password;
    public $role;
    public $city;

    public function __construct($db)
    {
        $this->conn = $db;
    }
}