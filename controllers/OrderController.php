<?php

class OrderController extends BaseController
{
    /***
     * "/order/create" endpoint
     * @return void
     */
    public function createAction(){
        // Сокздаем в БД новый заказ. Все товары из корзины перейдут в таблицу Состав заказа, где id заказа соответствует созданному заказу.
        sendOrderForDelivery();
    }

    public function sendOrderForDelivery(){
        // Отправляем заказ на доставку во внешнюю логистическую систему
    }
}